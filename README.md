# Gfinity Front-end Assignment

After installing the dependencies and serving the project, here are the links to each pages:

-   http://localhost:3000/
-   http://localhost:3000/search
-   http://localhost:3000/accept

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
